# WiiDI

An open source project to make Wii peripherals become powerful music making tools! Wii-MIDI! Currently supports: DRAWSOME! tablet.

WIRING:

Use either an i2c Wii peripheral breakout board (like this one https://www.adafruit.com/product/4836, which juuust so happens to pair perfectly with this microcontroller that has the perfect specs for our project https://www.adafruit.com/product/4600) OR cut off/ unscrew and extract wires/ solder new wires to peripheral PCB board and attach them as instructed.


YELLOW/DATA/SDA - > sda hardware pin on microcontroller


WHITE/CLOCK/SCL - > sdc hardware pin on microcontroller


BLACK/GND       - > ground pi on microcontroller


RED/3.3v/5v     - > 5v/3.3v pin on microcontroller. According to WiiBrew.org, peripherals are supposed to run on 3.3v, but the drawsome tablet also works 100% fine with 5v(???) If you can, use 3.3v but if you have to, use 5v.


I reccomend you first install 1.0 to debug, or just use an example from NintendExtensionCtrl to test it.
If that's working, just go ahead and upload v2.0, and configure the settings before uploading if you want.

Happy MIDI jams!

(I will soon be selling some prebuilt systems with many more features and software updates will be released to match. Definitley need to add some kind of button or serial interface soon, and of course add support for other controllers, but here's the initial project!)
