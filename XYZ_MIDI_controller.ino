#include <MIDIUSB.h>
#include <NintendoExtensionCtrl.h>
boolean aa = false;
boolean ab = false;
boolean ac = false;
int ad = 0;
boolean ba = false;
boolean bb = false;
boolean bc = false;
int bd = 0;
boolean ca = false;
boolean cb = false;
boolean cc = false;
int cd = 0;
int x = 0;

boolean EnableCC = true; //If True, the program will send CC controller values based on the setting below. However, Serial debug is disabled.
int channel = 0;          //The MIDI Channel to send the correspoding CC controller values on.
int CCx = 1;              //The CC controller the pen's "X" maps to.
int CCy = 2;              //The CC controller the pen's "Y" maps to.
boolean EnableZ = true;   //If true, the pressure of the pen tip will map to the specified CC controller.
int CCz = 3;              //If EnableZ is true, this is the CC controller the pen's tip pressure maps to.

int padx = 0;
int pady = 0;
boolean debugMode = false;
boolean justTriggered = false;

DrawsomeTablet tablet;


void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}


void setup() {
	Serial.begin(115200);  
	tablet.begin();
	//tablet.printDebug();
	while (!tablet.connect()) {
		delay(1);
	}
}

void loop() {
	boolean success = tablet.update();  
	if (!success) {
		delay(1);
		tablet.connect();
	}

	boolean detected = tablet.penDetected();
	uint16_t xCoordinate = tablet.penX();
	uint16_t yCoordinate = tablet.penY();
  uint16_t pressure = tablet.penPressure();
  int padx = map(xCoordinate, 0, 10206, 0, 127);
  int pady = map(yCoordinate, 0, 7422, 0, 127);
  int tip = constrain(map(pressure, 1000, 2047, 0, 127), 0, 127);

  if (padx >= 120 and pady <= 7 and tip >= 100 and justTriggered == false){
    debugMode = !debugMode;
    justTriggered = true;
  }
  if (padx < 100 or pady > 20){
    justTriggered = false; 
  }

  if (xCoordinate >= 9000 and yCoordinate >= 6000){
    aa = true;
  }
  else{
    aa = false;
  }
  if (aa == true){
    if (detected == true and pressure >= 2000 and ab == false){
      ab = true;
    }
    else if (detected == false and pressure <= 100 and ab == true){
      ab = false;
    }
  }
  if (aa == true and ab == true){
    ac = true;
  }
  if (aa == true and ab == false and ac == true){
    ad = ad + 1;
    ac = false;
  }
  if (aa == false){
    ab = false;
    ac = false;
    ad = 0;
  }
  if (xCoordinate <= 1000 and yCoordinate >= 6000){
    ba = true;
  }
  else{
    ba = false;
  }
  if (ba == true){
    if (detected == true and pressure >= 2000 and bb == false){
      bb = true;
    }
    else if (detected == false and pressure <= 100 and bb == true){
      bb = false;
    }
  }
  if (ba == true and bb == true){
    bc = true;
  }
  if (ba == true and bb == false and bc == true){
    bd = bd + 1;
    bc = false;
  }
  if (ba == false){
    bb = false;
    bc = false;
    bd = 0;
  }
  if (padx <= 10 and  pady <= 10){
    ca = true;
  }
  else{
    ca = false;
  }
  if (ca == true){
    if (detected == true and pressure >= 2000 and cb == false){
      cb = true;
    }
    else if (detected == false and pressure <= 100 and cb == true){
      cb = false;
    }
  }
  if (ca == true and cb == true){
    cc = true;
  }
  if (ca == true and cb == false and cc == true){
    cd = cd + 1;
    cc = false;
  }
  if (ca == false){
    cb = false;
    cc = false;
    cd = 0;
  }

  /*for (x = 0; x < 10; x++){
    Serial.println();
  }*/
  
  
  if (EnableCC == false or debugMode == true){
    tablet.printDebug();
    if (ad >= 6){
      Serial.println("A (X calibration)");
    }
    else if (bd >= 6){
      Serial.println("B (Y calibration)");
    }
    else if (cd >= 6){
      Serial.println("C (Z calibration)");
    }
    else{  
      Serial.println("No extra setting (Normal mode)");
    }
  }
  if (ad < 5 and bd < 5 and cd < 5 and EnableCC == true and detected == true and debugMode == false){
    controlChange(channel, CCx, padx); 
    controlChange(channel, CCy, pady); 
  }
  if (ad < 5 and bd < 5 and cd < 5 and EnableZ == true and EnableCC == true and detected == true and debugMode == false){
    controlChange(channel, CCz, tip);
  }
  if (ad >= 6){
     Serial.println("A (X calibration)");
     controlChange(channel, CCx, 0); 
     delay(100);
     controlChange(channel, CCx, 64); 
     delay(100);
     controlChange(channel, CCx, 127); 
     delay(100);
     controlChange(channel, CCx, 64); 
     delay(1000);
     controlChange(channel, CCx, 0); 
     delay(100);
     controlChange(channel, CCx, 64); 
     delay(100);
     controlChange(channel, CCx, 127); 
     delay(100);
     controlChange(channel, CCx, 64); 
     delay(1000);
     controlChange(channel, CCx, 0); 
     delay(100);
     controlChange(channel, CCx, 64); 
     delay(100);
     controlChange(channel, CCx, 127); 
     delay(100);
     controlChange(channel, CCx, 64); 
     delay(100);
     ad = 0;
     ab = false;
     ac = false;
     aa = false;
  }
  if (bd >= 6){
     Serial.println("B (Y calibration)");
     controlChange(channel, CCy, 0); 
     delay(100);
     controlChange(channel, CCy, 64); 
     delay(100);
     controlChange(channel, CCy, 127); 
     delay(100);
     controlChange(channel, CCy, 64); 
     delay(100);
     controlChange(channel, CCy, 0); 
     delay(1000);
     controlChange(channel, CCy, 64); 
     delay(100);
     controlChange(channel, CCy, 127); 
     delay(100);
     controlChange(channel, CCy, 64); 
     delay(100);
     controlChange(channel, CCy, 0); 
     delay(1000);
     controlChange(channel, CCy, 64); 
     delay(100);
     controlChange(channel, CCy, 127); 
     delay(100);
     controlChange(channel, CCy, 64); 
     delay(100);
     bd = 0;
     bb = false;
     bc = false;
     ba = false;
  }
  if (cd >= 6){
     Serial.println("C (Z calibration)");
     controlChange(channel, CCz, 0); 
     delay(100);
     controlChange(channel, CCz, 64); 
     delay(100);
     controlChange(channel, CCz, 127); 
     delay(100);
     controlChange(channel, CCz, 64); 
     delay(1000);
     controlChange(channel, CCz, 0); 
     delay(100);
     controlChange(channel, CCz, 64); 
     delay(100);
     controlChange(channel, CCz, 127); 
     delay(100);
     controlChange(channel, CCz, 64); 
     delay(1000);
     controlChange(channel, CCz, 0); 
     delay(100);
     controlChange(channel, CCz, 64); 
     delay(100);
     controlChange(channel, CCz, 127); 
     delay(100);
     controlChange(channel, CCz, 64); 
     delay(100);
     cb = false;
     cc = false;
     cd = 0;
     ca = false;
  }
  
}


	
	
