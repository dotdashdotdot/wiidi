#include <MIDIUSB.h>
#include <NintendoExtensionCtrl.h>
//                        MODE A DESCRIPTION: 
//                        A simple x/y/z pad, where "X" is left/right, "Y" is up/down, and "Z" is how hard you are pressing.
//                        MODE A PARAMETERS:
boolean EnableA = true;   //Whether or not mode a will be running (NOTE: you CAN have mode A AND B AT THE SAME TIME!!! THEY ARE NOT EXCLUSIVE!!!)
int channelA= 1;          //The MIDI Channel to send the correspoding MIDI messages on.
boolean EnableX = true;   //If true, the "X" of the pen will map to the specified CC controller.
int CCx = 1;              //The CC controller the pen's "X" maps to.
boolean EnableY = true;   //If true, the "Y" of the pen will map to the specified CC controller.
int CCy = 2;              //The CC controller the pen's "Y" maps to.
boolean EnableZ = true;   //If true, the pressure of the pen tip will map to the specified CC controller.
int CCz = 3;              //The CC controller the pen's tip pressure maps to.

//                        MODE B DESCRIPTION:
//                        A mode where the Y is velocity, the pressure is note on/off, and the X is the midi note.
//                        MODE B PARAMETERS:
boolean EnableB = true;   //Whether or not mode b will be running (NOTE: you CAN have mode A AND B AT THE SAME TIME!!! THEY ARE NOT EXCLUSIVE!!!)
int channelB = 2;         //The MIDI Channel to send the correspoding MIDI messages on.
int upperx = 72;          //The highest note the pad will play.
int lowerx = 60;          //The lowest note the pad will play.
int uppery = 50;          //The maximum velocity the pad will play (BUT!!! if you want the top to be the max velocity and 
                          //the bottom to be the minimum, INVERT THE POSITIOINS OF THE UPPER AND LOWER BOUNDS LIKE THE EXAMPLE!!!!!)
int lowery = 127;         //The minimum velocity the pad will play (BUT!!! if you want the top to be the max velocity and 
                          //the bottom to be the minimum, INVERT THE POSITIOINS OF THE UPPER AND LOWER BOUNDS LIKE THE EXAMPLE!!!!!)
int threshold = 50;       //The pressure threshold at which a note is played/stopped.

//THE CODE(OH MY GOD YES I KNOW ITS A MESS TRUST ME THIS IS BETTER BY ABOUT 2.5 TIMES AS EFFICIENT THAT 1.0 (68% of program space for just mode a) AND IT'S STILL INSANE SORRY!!!!)

//definitions of var-ious variables (not sorry LOL)
boolean detected = false;
uint16_t xCoordinate = 0;
uint16_t yCoordinate = 0;
uint16_t pressure = 0;
uint16_t padx = 0;
uint16_t padxa = 0;
uint16_t padxb = 0;
uint16_t pady = 0;
uint16_t padya = 0;
uint16_t tip = 0;
boolean playing = false;
DrawsomeTablet tablet;


//Standard functions for MIDI implementation
void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}
void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
  MidiUSB.flush();
}
void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
  MidiUSB.flush();
}

//Simply initialises the tablet.
void setup() {
	tablet.begin();
	while (!tablet.connect()) {
		delay(1);
	}
}
 
//Mode B MIDI generation code.
void modeA(int padx, int CCx, int pady, int CCy, int EnableZ, int tip, int CCz, int channelA) {
  if (EnableX == true){
    controlChange(channelA, CCx, padx); 
  }
  if (EnableY == true){
    controlChange(channelA, CCy, pady); 
  }
  if (EnableZ == true){
    controlChange(channelA, CCz, tip);
  }
}
//Mode B MIDI generation code.
void modeB(int padx, int pady, int tip, int channelB, int upperx, int lowerx, int lowery, int uppery, int threshold) {
  if (tip >= threshold and playing == false){
    padxa = map(padx, 0, 127, lowerx, upperx);
    padya = map(pady, 0, 127, lowery, uppery);
    noteOn(channelB, padxa, padya);
    padxb = padxa;
    playing = true;
  }
  if (tip < threshold and playing == true){
    noteOff(channelB, padxb, padya);
    playing = false;
  }
}

//Main functions.
void loop() {
  //Get data from tablet
  boolean success = tablet.update();  
  if (!success) {
    delay(1);
    tablet.connect();
  }
  //Process new data from tablet 
  boolean detected = tablet.penDetected();
  uint16_t xCoordinate = tablet.penX();
  uint16_t yCoordinate = tablet.penY();
  uint16_t pressure = tablet.penPressure();
  
  //Convert big number into the range of MIDI values
  int padx = map(xCoordinate, 0, 10206, 0, 127);
  int pady = map(yCoordinate, 0, 7422, 0, 127);
  int tip = map(pressure, 1000, 2048, 0, 127);
  
  //...And the simply call A and B (if active, of course)
  if (EnableA){
    modeA(padx, CCx, pady, CCy, EnableZ, tip, CCz, channelA);
  }
  if (EnableB){
    modeB(padx, pady, tip, channelB, upperx, lowerx, lowery, uppery, threshold);
  }
}
